const fs = require("fs")

const createDirectory = (path, cb) => {
    fs.mkdir(path, cb)
}

const createFile = (path, data, cb) => {
    if (path && data && cb) {
        fs.writeFile(path, data, cb)
    }
}

const deleteFile = (path, cb) => {
    if (path && cb) {
        fs.unlink(path, cb)
    }
}

module.exports = { createDirectory, createFile, deleteFile }