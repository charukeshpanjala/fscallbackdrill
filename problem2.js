const fs = require("fs")
const path = require("path")

const getPath = (fileName) => path.join(__dirname, fileName)

const readFile = (path, cb) => {
    if (path && cb) {
        fs.readFile(getPath(path), "utf8", cb)
    }
}

const convertContentToUpperCase = (newFile, data, cb) => {
    const upperData = (data.toUpperCase())
    fs.writeFile(newFile, upperData, () => {
        console.log(`${newFile} created.`)
        fs.appendFile(getPath("filenames.txt"), newFile + ",", () => {
            console.log(`${newFile} added to filenames.txt`)
            cb(upperData)
        })
    })
}

const convertToLowerCaseAndSplit = (newFile, data, cb) => {
    const newData = data.toLowerCase().replaceAll(". ", "\n")
    fs.writeFile(newFile, newData, () => {
        console.log(`${newFile} created.`)
        fs.appendFile(getPath("filenames.txt"), newFile + ",", () => {
            console.log(`${newFile} added to filenames.txt`)
            cb(newData)
        })
    })
}

const sortData = (newFile, data, cb) => {
    const sortedData = (data).split("\n").sort()
    fs.writeFile(newFile, sortedData.toString(), () => {
        console.log(`${newFile} created.`)
        fs.appendFile(getPath("filenames.txt"), newFile, () => {
            console.log(`${newFile} added to filenames.txt`)
            cb()
        })
    })
}

const deleteNewFiles = (cb) => {
    readFile("filenames.txt", (err, data) => {
        if (err) {
            console.log(err)
        }
        const names = data.split(",")
        names.forEach(element => {
            const elementPath = path.resolve(element)
            fs.unlink(elementPath, () => {
                cb(element)
            })
        });
    })
}
module.exports = { readFile, convertContentToUpperCase, convertToLowerCaseAndSplit, sortData, deleteNewFiles }