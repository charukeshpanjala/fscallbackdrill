const { createDirectory, createFile, deleteFile } = require("../problem1")
const path1 = "./jsonFiles/name.JSON"
const path2 = "./jsonFiles/profession.JSON"
const path3 = "./jsonFiles/age.JSON"
const path4 = "./jsonFiles/place.JSON"

const cb = (names) => {
    if (names.length === 4) {
        names.forEach(element => {
            deleteFile(element, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        });
        console.log("All files deleted")
    }
}


createDirectory("jsonFiles", (err) => {
    if (err) {
        console.log(err)
    } else {
        let names = []
        createFile(path1, "Jo", (err) => {
            if (err) {
                console.log(err)
            } else {
                names.push(path1)
                console.log(`${path1} created`)
                cb(names)
            }
        });
        createFile(path2, "SDE", (err) => {
            if (err) {
                console.log(err)
            } else {
                names.push(path2)
                console.log(`${path2} created`)
                cb(names)

            }
        });
        createFile(path3, "25", (err) => {
            if (err) {
                console.log(err)
            } else {
                names.push(path3)
                console.log(`${path1} created`)
                cb(names)

            }
        });
        createFile(path4, "Banglore", (err) => {
            if (err) {
                console.log(err)
            } else {
                names.push(path4)
                console.log(`${path1} created`)
                cb(names)
            }
        });
    }
})