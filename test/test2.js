const { readFile, convertContentToUpperCase, convertToLowerCaseAndSplit, sortData, deleteNewFiles } = require("../problem2")

readFile("lipsum.txt", (err, data) => {
    convertContentToUpperCase("upperCaseData.txt", data, (upperData) => {
        convertToLowerCaseAndSplit("lowerCaseSplitData.txt", upperData, (lowerData) => {
            sortData("sortedData.txt", lowerData, () => {
                deleteNewFiles((fileName) => {
                    console.log(`${fileName} deleted successfully.`)
                })
            })
        })
    })
})